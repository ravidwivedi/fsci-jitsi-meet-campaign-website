# Website files of FSCI Jitsi Meet Crowdfunding Campaign
_Forked from [FSCI's main site](https://gitlab.com/fsci/fsci.gitlab.io)_

Website at [https://fund.fsci.in](https://fund.fsci.in).

### Usage

It's good to have basic knowledge of Hugo, git & markdown. Don't worry, It's easy to learn them :-)

This [hugo tutorial playlist](https://invidio.us/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) will help you to understand hugo easily.

- Most of changes can be achieved via changing `index.html` file in `/layouts` directory.
- Navbar changes in `nav.html` file in `/layouts/partials` directory.
- Footer done via `footer.html` file in `/layouts/partials` directory.
- Contributors can be added by changing `contributors.md` file under `/content/english/` directory.

For making Peertube embeds, replace `[..]/watch/[..]` in video URL with `[..]/embed/[..]`. <br>
*ex - embed URL for https://video.blender.org/videos/watch/264ff760-803e-430e-8d81-15648e904183 would be https://video.blender.org/videos/embed/264ff760-803e-430e-8d81-15648e904183*
