---
title: "Why Support?"
date: 2020-06-13T17:13:37+05:30
author: "false"
draft: false
translationKey: why-support
---

After the Covid-19 pandemic, we're having our virtual meets via the internet and the need to communicate has only increased.

Currently, most one on one personal chats and group meetings are conducted using "free = gratis" proprietary services like WhatsApp, Google Meet, Zoom, Microsoft Teams, Skype, etc. which collect and aggregate digital data and metadata.

This data is then processed and combined with other information collected about you from services that you use including data from retailers, credit card providers, medical records bought from data brokers to build an online profile on you. Also, the proprietary services are notorious for hiding security flaws and suffer from data breaches, spilling one's data over the internet. The profile they build is very valuable to advertisers, companies providing services like insurance or loans, political parties, ruling governments among others.

With you spending most of your time online, they target you online with this profile and trap you in an online bubble with the data you knowingly or unknowingly handed over. Limited online usage inside a bubble, with algorithms deciding what you see, affects your agency of choice and freedom.

It is therefore important to own our means of digital communications whether you are an individual or an organization. Free Software video conferencing services like Jitsi, which you can inspect, learn, modify, redistribute and set up yourself, will give you that ownership, freedom and privacy of your data. Jitsi is a secure, fully-featured video conferencing solution that can be used over mobile devices and desktop. It's as simple as sharing a meeting link and anyone can join without an account or installation. If you don't want to set up and run the service yourself you can use the jitsi service set up by someone you trust.

By running a Jitsi instance at [meet.fsci.in](https://meet.fsci.in), Free Software Community of India (FSCI) intends to provide you that freedom without handing your data over to anyone. Your data is private and secure with no one snooping on your conversations. This service, like others, from FSCI,. is set up and maintained by a group of volunteers who want to spread the message of software freedom to everyone while protecting their privacy.

### If you are interested in using the service and the ideals behind running it, donate towards the crowdfunding target amount of 62,500 INR which will cover the cost of running it on a server for 24 months. 

### [Donate to the campaign](/#Donate)

