---
title: "Contributors"
---

List of all the contributors to FSCI's Jitsi Meet campaign (in INR).

- Pirate Praveen 3,000
- Ravi Dwivedi 100
- N J Kiran Babu 2500
- vik 16440 (after convertion and paypal fee, actual $250)
- Sahil Dhiman 1200
- Arun Mathai SK 200
- Joice M Joseph 1000
- Raghu Kamath 2000
- Sooraj 3000
- Snehal 2500
- Ravi 400
- Chandrap 10
- Jesvin 2000
- Vinod 500
- Kelvin 1001


<br> <br>
**Recieved amount** - 35,851 INR <br>
**Target amount** - 62,500 INR <br>
**Last updated** - 08/07/21


### We are very grateful to our contributors for supporting the ideals behind running this service.
<br>

### Credits to Volunteers

- **Video** : abbyck, Arun Mathai SK, Ayesha Gohar, Ravi, Riya Sawant, Sayan, Rojin, Bourasi, Vennala for the contributing face and voice to the video.
- **Content** : Akhil Varkey, sahilister, Anupa Ann Joseph, Pirate Bady, Ravi and all others who have edited the pad and content.
- **Video Editing** : mujeebcpy, Noorul.
- **Making poster** : Riya Sawant, Raghu Kamath.
- **Website** : Akhil Varkey, sahilister , Pirate Bady.
- **Providing bank account for receiving donations** : hacksk.
- **Jitsi setup** : sahilister, Mridul, insaanimanav, hacksk.
- **Coordinating the whole event** : Akhil Varkey.
And thanks to all who have commented from the very initial part of the campaign.

Thanks Pirate ‍ Praveen for kickstarting the campaign with the initial amount.

<br>
<br>
<br>
<br>
<br>
<br>
<br>

 
